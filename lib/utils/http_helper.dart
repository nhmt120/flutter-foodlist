import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:food_list/models/pizza.dart';

class HttpHelper {
  final String server = 'flutter-pizza.mocklab.io';
  final String path = 'pizza-list';
  final String postPath = 'pizza';
  final String putPath = 'pizza';
  final String deletePath = 'pizza';

  Future<List<Pizza>> getPizzaList() async {
    Uri url = Uri.https(server, path);
    http.Response result = await http.get(url);

    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      //provide a type argument to the map method to avoid type error
      List<Pizza> pizzas =
          jsonResponse.map<Pizza>((p) => Pizza.fromJson(p)).toList();
      return pizzas;
    } else {
      return [];
    }
  }

  Future<http.Response> postPizza(Pizza pizza) async {
    final jsonPost = json.encode(pizza.toJson());
    Uri url = Uri.https(server, postPath);

    return http.post(url, body: jsonPost);
  }

  Future<http.Response> putPizza(Pizza pizza) async {
    final jsonPost = json.encode(pizza.toJson());
    Uri url = Uri.https(server, postPath);

    return http.put(url, body: jsonPost);
  }
}
