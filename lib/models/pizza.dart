const keyId = 'id';
const keyName = 'pizzaName';
const keyDescription = 'description';
const keyPrice = 'price';
const keyImage = 'imageUrl';

class Pizza {
  late int id;
  late String pizzaName;
  late String description;
  late double price;
  late String imageUrl;

  Pizza.fromJson(Map<String, dynamic> json) {
    id = json[keyId] ?? 0;
    pizzaName = json[keyName] ?? '';
    description = json[keyDescription] ?? '';
    price = json[keyPrice] ?? 0.0;
    imageUrl = json[keyImage] ?? '';
  }


  Pizza(this.id, this.pizzaName, this.description, this.price, this.imageUrl);

  Map<String, dynamic> toJson() {
    return {
      keyId: id,
      keyName: pizzaName,
      keyDescription: description,
      keyPrice: price,
      keyImage: imageUrl,
    };
  }
}
