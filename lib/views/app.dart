import 'package:flutter/material.dart';

import 'create_pizza_screen.dart';
import 'home_screen.dart';
import 'setting_screen.dart';
import 'platform_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FoodListApp extends StatelessWidget {
  const FoodListApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Food App',
      theme: ThemeData(
          primarySwatch: Colors.deepOrange,
          visualDensity: VisualDensity.adaptivePlatformDensity),
      // home: MyHomePage(),
      routes: {
        '/': (context) => MyHomePage(),
        '/add_pizza': (context) => CreatePizzaScreen()
      },
      initialRoute: '/',
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int selectedIndexBottomMenu = 0;
  final screens = [
    HomeScreen(),
    SettingScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Food List'),
        actions: [_addPizza()],
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.notifications),
        onPressed: () async {
          // Get stored appCounter
          SharedPreferences prefs = await SharedPreferences.getInstance();

          var appCounter = prefs.getInt('appCounter') ?? 0;

          final alert = PlatformAlert(
            title: 'Times of opened app',
            message: 'Your have opened the app is $appCounter times.',
          );
          alert.show(context);
        },
      ),
      body: IndexedStack(
        children: screens,
        index: selectedIndexBottomMenu,
      ),
      bottomNavigationBar: _buildBottomMenu(),
    );
  }

  BottomNavigationBar _buildBottomMenu() {
    return BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          label: 'Settings',
        ),
      ],
      currentIndex: selectedIndexBottomMenu,
      onTap: _onBottomMenuTap,
    );
  }

  void _onBottomMenuTap(int value) {
    setState(() {
      if (value == 1) {
        readAndWritePreference();
      }
      selectedIndexBottomMenu = value;
    });
  }

  readAndWritePreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var appCounter = prefs.getInt('appCounter') ?? 0;
    appCounter++;
    prefs.setInt('appCounter', appCounter);
    setState(() {});
  }

  Widget _addPizza() {
    return IconButton(
      onPressed: () {
        Navigator.of(context).pushReplacementNamed('/add_pizza');
      },
      icon: const Icon(Icons.add),
      tooltip: 'Add Pizza',
    );
  }
}
