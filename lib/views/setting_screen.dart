import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SettingScreenState();
  }
}

class _SettingScreenState extends State<StatefulWidget> {
  late int appCounter; // = 0;

  String documentsPath = '';
  String tempPath = '';

  late File myFile;
  String fileText = '';

  @override
  void initState() {
    readAndWritePreference();
    getPaths().then((_) {
      myFile = File('$documentsPath/pizzas.txt');
      writeFile();
    });
    super.initState();
  }

  // @override
  // void didUpdateWidget(StatefulWidget oldWidget) {
  //   super.didUpdateWidget(oldWidget);
  //   readAndWritePreference();
  // }

  @override
  Widget build(BuildContext context) {
    readAndWritePreference();
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('The app has been opened $appCounter times.'),
            Text('Document path: $documentsPath'),
            Text('Temp path: $tempPath'),
            ElevatedButton(
              child: Text('Read File'),
              onPressed: () => readFile(),
            ),
            Text(fileText),
          ],
        ),
      ),
    );
  }

  Future readAndWritePreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    appCounter = prefs.getInt('appCounter') ?? 0;
    // appCounter++;

    // prefs.setInt('appCounter', appCounter);
    // await prefs.setInt('appCounter', appCounter);
    setState(() {});
  }

  Future getPaths() async {
    final docDir = await getApplicationDocumentsDirectory();
    final tempDir = await getTemporaryDirectory();
    setState(() {
      documentsPath = docDir.path;
      tempPath = tempDir.path;
    });
  }

  Future<bool> writeFile() async {
    try {
      await myFile.writeAsString('Margherita, Capricciosa and Napoli');
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> readFile() async {
    try {
      // Read the file.
      String fileContent = await myFile.readAsString();
      setState(() {
        fileText = fileContent;
      });
      return true;
    } catch (e) {
      // On error, return false.
      return false;
    }
  }
}
