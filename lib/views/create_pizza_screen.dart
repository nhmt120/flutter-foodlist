import 'package:flutter/material.dart';
import 'package:food_list/models/pizza.dart';
import 'package:food_list/utils/http_helper.dart';

class CreatePizzaScreen extends StatefulWidget {
  const CreatePizzaScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CreatePizzaScreenState();
  }
}

class CreatePizzaScreenState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();

  var nameController = TextEditingController();
  var descriptionController = TextEditingController();
  var priceController = TextEditingController();

  String postMessage = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushReplacementNamed('/');
          },
          icon: const Icon(Icons.arrow_back),
        ),
        title: const Text('New pizza'),
      ),
      body: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              _fieldName(),
              _fieldDescription(),
              _fieldPrice(),
              _buttonSave(),
              Text(postMessage),
            ],
          ),
        ),
      ),
    );
  }

  Widget _fieldName() {
    return TextFormField(
      controller: nameController,
      decoration: const InputDecoration(
        labelText: 'Name',
      ),
    );
  }

  Widget _fieldDescription() {
    return TextFormField(
      controller: descriptionController,
      keyboardType: TextInputType.multiline,
      decoration:
          const InputDecoration(labelText: 'Description', helperMaxLines: 3),
    );
  }

  Widget _fieldPrice() {
    return TextFormField(
      controller: priceController,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(labelText: 'Price'),
    );
  }

  Widget _buttonSave() {
    return ElevatedButton(
        onPressed: () {
          _savePizza();
        },
        child: const Text('Save'));
  }

  void _savePizza() async {
    String name = nameController.text;
    String description = descriptionController.text;
    print(priceController.text);
    // double price = double.parse(priceController.text);

    var httpHelper = HttpHelper();
    var pizza = Pizza(111, name, description, 5, 'None');

    var response = await httpHelper.postPizza(pizza);

    if (response.statusCode == 201) {
      postMessage = 'Pizza added successfully.';
      print('Response message = ${response.body}');
    } else {
      postMessage = 'Add pizza failed.';
      print('Response message = ${response.body}');
    }
    setState(() {

    });
  }
}
