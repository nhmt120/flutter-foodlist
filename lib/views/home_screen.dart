import 'package:flutter/material.dart';
import 'package:food_list/utils/http_helper.dart';
import 'package:food_list/views/update_pizza_screen.dart';
import 'dart:convert';
import '../models/pizza.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pizza List'),
        // actions: [
        //   _addPizza()
        // ],
      ),
      body: FutureBuilder(
        future: callPizzas(),
        builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
          return ListView.builder(
            itemCount: pizzas.data?.length ?? 0,
            itemBuilder: (BuildContext context, int index) {
              var pizza = pizzas.data![index];
              return Dismissible(
                key: ValueKey(pizza),
                background: Container(color: Colors.red),
                direction: DismissDirection.endToStart,
                onDismissed: (_) {
                  // TODO Delete Pizza
                },
                child: ListTile(
                  title: Text(pizza.pizzaName),
                  subtitle: Text("${pizza.description} \$ ${pizza.price}"),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => UpdatePizzaScreen(),
                        settings: RouteSettings(
                          arguments: pizza,
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }

  // Widget _addPizza() {
  //   return IconButton(
  //     onPressed: () {
  //       Navigator.of(context).pushReplacementNamed('/add_pizza');
  //     },
  //     icon: Icon(Icons.add),
  //     tooltip: 'Add Pizza',
  //   );
  // }

  Future<List<Pizza>> callPizzas() async {
    HttpHelper helper = HttpHelper();
    List<Pizza> pizzas = await helper.getPizzaList();
    return pizzas;
  }

  Future<List<Pizza>> readJsonFile(context) async {
    String myString = await DefaultAssetBundle.of(context)
        .loadString('assets/pizza_list.json');

    List myMap = jsonDecode(myString);
    List<Pizza> myPizzas = [];
    for (var pizza in myMap) {
      Pizza myPizza = Pizza.fromJson(pizza);
      // Pizza myPizza = Pizza.fromJsonOrNull(pizza);
      myPizzas.add(myPizza);
    }
    return myPizzas;
  }
}
