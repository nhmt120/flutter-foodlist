import 'package:flutter/material.dart';
import 'package:food_list/models/pizza.dart';
import 'package:food_list/utils/http_helper.dart';

class UpdatePizzaScreen extends StatefulWidget {
  const UpdatePizzaScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return UpdatePizzaScreenState();
  }

}

class UpdatePizzaScreenState extends State<UpdatePizzaScreen> {
  final formKey = GlobalKey<FormState>();

  var nameController = TextEditingController();
  var descriptionController = TextEditingController();
  var priceController = TextEditingController();

  String putMessage = '';

  @override
  Widget build(BuildContext context) {
    Pizza pizza = ModalRoute.of(context)!.settings.arguments as Pizza;

    nameController = TextEditingController(text: pizza.pizzaName);
    descriptionController = TextEditingController(text: pizza.description);
    priceController = TextEditingController(text: pizza.price.toString());

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushReplacementNamed('/');
          },
          icon: const Icon(Icons.arrow_back),
        ),
        title: const Text('Update pizza'),
      ),
      body: Form(
        key: formKey,
        child: Column(
          children: [
            _fieldName(),
            _fieldDescription(),
            _fieldPrice(),
            _buttonUpdate(),
            Text(putMessage),
          ],
        ),
      ),
    );
  }

  Widget _fieldName() {
    return TextFormField(
      // initialValue: pizzaName,
      controller: nameController,
      decoration: const InputDecoration(
        labelText: 'Name',
      ),
    );
  }

  Widget _fieldDescription() {
    return TextFormField(
      // initialValue: description,
      controller: descriptionController,
      keyboardType: TextInputType.multiline,
      decoration:
      const InputDecoration(labelText: 'Description', helperMaxLines: 3),
    );
  }

  Widget _fieldPrice() {
    return TextFormField(
      // initialValue: price.toString(),
      controller: priceController,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(labelText: 'Price'),
    );
  }

  Widget _buttonUpdate() {
    return ElevatedButton(
        onPressed: () {
          _updatePizza();
        },
        child: const Text('Update'));
  }

  void _updatePizza() async {
    String name = nameController.text;
    String description = descriptionController.text;
    String price = priceController.text;

    var httpHelper = HttpHelper();
    var pizza = Pizza(111, name, description, double.tryParse(price)!, 'None');

    var response = await httpHelper.putPizza(pizza);

    if (response.statusCode == 201) {
      putMessage = response.body;
      print('Response message = ${response.body}');
    } else {
      putMessage = response.body;
      print('Response message = ${response.body}');
    }
    setState(() {

    });
  }
}
