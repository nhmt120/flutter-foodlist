import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:food_list/models/pizza.dart';
import 'package:food_list/utils/http_helper.dart';

void main() {
  var httpHelper = HttpHelper();
  test('Test get list of pizzas.', () async {
    var pizzaList = await httpHelper.getPizzaList();

    expect(pizzaList.isEmpty, false);
    print(pizzaList);
  });

  test('Test post pizza to server.', () async {
    var pizza = Pizza(111, 'Milk1 Pi', 'None', 12, 'None');
    var response = await httpHelper.postPizza(pizza);

    expect(response.statusCode, 201);
    var jsonBody = jsonDecode(response.body);
    expect(jsonBody['message'], 'New pizza added successfully.');
  });
}
